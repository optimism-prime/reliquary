from math import exp

initial_offset = 0.1
initial_speed = 0.5
middle_speed = 0.2
final_speed = initial_speed

def compute_level_multipliers() -> list:
    level_multipliers = []
    for index in range(52):
        if index < 13:
            x = exp(index * initial_speed)
            level_multipliers.append(x / (x + 1) - 0.5 + initial_offset)
        elif index < 39:
            x = exp((index - 13) * middle_speed)
            level_multipliers.append(x / (x + 1) - 0.5 + level_multipliers[12])
        else:
            x = exp((index - 39) * final_speed)
            level_multipliers.append(x / (x + 1) - 0.5 + level_multipliers[38])
    return level_multipliers

def rescale(level_multipliers: list, max_int: int, minimum_delta: int):
    rescaled_level_multipliers = []
    max_value = level_multipliers[-1]
    for value in level_multipliers:
        rescaled_level_multipliers.append(round(max_int * value / max_value))
    offset_accum = 0
    offsets = [0]
    for i in range(1, len(rescaled_level_multipliers)):
        offset = minimum_delta if rescaled_level_multipliers[i] == rescaled_level_multipliers[i - 1] else 0
        offset_accum += offset
        offsets.append(offset_accum)
    for i in range(len(rescaled_level_multipliers)):
        rescaled_level_multipliers[i] = min(rescaled_level_multipliers[i] + offsets[i], max_int)
    return rescaled_level_multipliers

print("requiredMaturities:")
print(",".join([str(x) for x in [7 * 24 * 3600 * i for i in range(52)]]))
print()
print("levelMultipliers:")
print(",".join([str(x) for x in rescale(compute_level_multipliers(), 1000, 1)]))
