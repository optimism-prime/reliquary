import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";

// For safety checks:
const networks = {
  deployFantom: {
    chainId: 250,
  },
} as { [key: string]: { [key: string]: any } };

const deploy: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const network = await ethers.provider.getNetwork();
  if (!(hre.network.name in networks)) {
    console.error(
      `Cannot deploy to network '${hre.network.name}'. Only 'deployFantom' is supported.`,
    );
    process.exit(1);
  }

  const currentNetworkInfo = {
    chainId: network.chainId,
  };
  const expectedNetworkInfo = networks[hre.network.name];

  if (
    currentNetworkInfo.chainId != expectedNetworkInfo.chainId
  ) {
    console.log(
      `Wrong network detected to deploy to, expected ${JSON.stringify(expectedNetworkInfo)}, got ${JSON.stringify(
        currentNetworkInfo,
      )}. Please connect to the correct network and set the good target on command line.`,
    );
    process.exit(1);
  }

  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const curveDeployResult = await deploy("003_Autobribes_Equalizer_WFTMRewards_OwnableCurve", {
    contract: "OwnableCurve",
    from: deployer,
    args: [
      0,
    ],
    log: true,
  });
  if (curveDeployResult.newlyDeployed) {
    await hre.run("verify:verify", {
      address: curveDeployResult.address,
      constructorArguments: curveDeployResult.args,
    });
  }
  const reliquaryDeployResult = await deploy("003_Autobribes_Equalizer_WFTMRewards_ReliquaryAutobribes", {
    contract: "ReliquaryAutobribesEqualizer",
    from: deployer,
    args: [
      "0x21be370D5312f44cB42ce377BC9b8a0cEF1A4C83", // Reward token: WFTM https://ftmscan.com/token/0x21be370d5312f44cb42ce377bc9b8a0cef1a4c83
      curveDeployResult.address,
      "Autobribes_Equalizer_WFTMRewards",
      "RELIC_AUTOBRIBES_EQUALIZER_FTM",
      "0x3Fd3A0c85B70754eFc07aC9Ac0cbBDCe664865A6", // EQUAL: https://ftmscan.com/token/0x3fd3a0c85b70754efc07ac9ac0cbbdce664865a6
    ],
    log: true,
  });
  if (reliquaryDeployResult.newlyDeployed) {
    await hre.run("verify:verify", {
      address: reliquaryDeployResult.address,
      constructorArguments: reliquaryDeployResult.args,
    });
  }
};
export default deploy;
