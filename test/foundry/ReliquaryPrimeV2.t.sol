// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "contracts/ReliquaryPrimeV2Velodrome.sol";
import "contracts/ReliquaryPrimeV2Equalizer.sol";
import "contracts/emission_curves/Constant.sol";
import "contracts/nft_descriptors/NFTDescriptor.sol";
import "openzeppelin-contracts/contracts/token/ERC721/utils/ERC721Holder.sol";
import "openzeppelin-contracts/contracts/mocks/ERC20DecimalsMock.sol";
import "openzeppelin-contracts/contracts/mocks/ERC721Mock.sol";
import "openzeppelin-contracts/contracts/mocks/ERC1155Mock.sol";

abstract contract ReliquaryPrimeV2Test is ERC721Holder, Test {
    ReliquaryPrimeV2Base reliquary;

    uint constant SINGLE_POOL_ID = 0;
    uint constant PAIR_POOL_ID = 1;

    ERC20DecimalsMock rewardToken;
    ERC20DecimalsMock pairToken;
    ERC20DecimalsMock singleToken;
    ERC20DecimalsMock farmingToken;
    address nftDescriptor;

    uint[] requiredMaturity = [0, 1 days, 7 days, 14 days, 30 days, 90 days, 180 days, 365 days];
    uint[] allocPoints = [100, 120, 150, 200, 300, 400, 500, 750];

    address gauge;

    function setUp() public {
        rewardToken = new ERC20DecimalsMock("Reward", "REWARD", 18);
        pairToken = new ERC20DecimalsMock("Pair", "PAIR", 18);
        singleToken = new ERC20DecimalsMock("Single", "SINGLE", 18);
        farmingToken = new ERC20DecimalsMock("Staking Reward", "STKREWARD", 18);

        setUpReliquary();

        nftDescriptor = address(new NFTDescriptor(address(reliquary)));

        reliquary.grantRole(keccak256("OPERATOR"), address(this));
        reliquary.addPool(
            100, address(singleToken), address(0), requiredMaturity, allocPoints, "SINGLE Pool", nftDescriptor, true
        );
        reliquary.addPool(
            200, address(pairToken), address(0), requiredMaturity, allocPoints, "SINGLE Pool", nftDescriptor, true
        );

        reliquary.setGaugeOf(PAIR_POOL_ID, gauge);

        rewardToken.mint(address(this), 100_000_000 ether);
        farmingToken.mint(address(this), 100_000_000 ether);

        singleToken.mint(address(this), 100_000_000 ether);
        singleToken.approve(address(reliquary), type(uint).max);

        pairToken.mint(address(this), 100_000_000 ether);
        pairToken.approve(address(reliquary), type(uint).max);
    }

    function setUpReliquary() virtual internal;
    function makeGauge(address stakingToken) virtual internal returns (address);

    function testPairTokenStakedWhenCreateRelicAndDeposit(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        // We expect amount pairToken to have been staked in the gauge
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(gauge), amount);
    }

    function testPairTokenStakedWhenDepositExisting(uint amountA, uint amountB) public {
        amountA = bound(amountA, 1, type(uint).max / 2);
        amountB = bound(amountB, 1, type(uint).max / 2);
        vm.assume(amountA + amountB <= pairToken.balanceOf(address(this)));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountA);
        reliquary.deposit(amountB, relicId);
        assertEq(reliquary.getPositionForId(relicId).amount, amountA + amountB);

        // We expect amountA + amountB pairTokens to have been staked in the gauge
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(gauge), amountA + amountB);
    }

    function testSingleTokenNotStakedWhenCreateRelicAndDeposit(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        // We expect amount singleToken to remain in the reliquary
        assertEq(singleToken.balanceOf(address(reliquary)), amount);
    }

    function testThatSetGaugeOfWithdrawAllAndStakeInNewGaugeAndOldGaugesCanBeClaimed(
        uint amountA,
        uint amountB,
        uint amountStakingReward
    ) public {
        amountA = bound(amountA, 1, type(uint).max / 2);
        amountB = bound(amountB, 1, type(uint).max / 2);
        amountStakingReward = bound(amountB, 0, type(uint).max / 2);
        vm.assume(amountA + amountB <= pairToken.balanceOf(address(this)));
        vm.assume(amountStakingReward <= farmingToken.balanceOf(address(this)));

        address localGauge = makeGauge(address(pairToken));

        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountA);

        farmingToken.transfer(gauge, amountStakingReward);
        uint farmingTokenBalance = farmingToken.balanceOf(address(this));

        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(gauge), amountA);

        reliquary.setGaugeOf(PAIR_POOL_ID, address(0));
        assertEq(pairToken.balanceOf(address(reliquary)), amountA);
        assertEq(pairToken.balanceOf(gauge), 0);

        assertEq(farmingToken.balanceOf(address(this)), farmingTokenBalance);
        assertEq(farmingToken.balanceOf(gauge), amountStakingReward);

        address[] memory tokens = new address[](1);
        tokens[0] = address(farmingToken);
        reliquary.harvestGaugeRewards(gauge, tokens);
        assertEq(farmingToken.balanceOf(address(this)), farmingTokenBalance + amountStakingReward);
        assertEq(farmingToken.balanceOf(gauge), 0);

        reliquary.setGaugeOf(PAIR_POOL_ID, address(localGauge));
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(address(localGauge)), amountA);
        assertEq(pairToken.balanceOf(gauge), 0);

        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountB);
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(address(localGauge)), amountA + amountB);
        assertEq(pairToken.balanceOf(gauge), 0);
    }

    function testSingleTokenNotStakedWhenDepositExisting(uint amountA, uint amountB) public {
        amountA = bound(amountA, 1, type(uint).max / 2);
        amountB = bound(amountB, 1, type(uint).max / 2);
        vm.assume(amountA + amountB <= singleToken.balanceOf(address(this)));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amountA);
        reliquary.deposit(amountB, relicId);
        assertEq(reliquary.getPositionForId(relicId).amount, amountA + amountB);

        // We expect amountA + amountB singleTokens to remain in the reliquary
        assertEq(singleToken.balanceOf(address(reliquary)), amountA + amountB);
    }

    function testPairTokenUnstakedWhenWithdraw(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdraw(amount, relicId);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testPairTokenUnstakedWhenWithdrawAndHarvest(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdrawAndHarvest(amount, relicId, gauge);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenWithdraw(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdraw(amount, relicId);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenWithdrawAndHarvest(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdrawAndHarvest(amount, relicId, gauge);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenEmergencyWithdraw(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.emergencyWithdraw(relicId);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testPairTokenEmergencyWithdraw(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.emergencyWithdraw(relicId);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testRevertOnGaugeStakeTokenMismatch() public {
        vm.expectRevert(ReliquaryPrimeV2Base.GaugeStakeTokenMismatch.selector);
        reliquary.setGaugeOf(SINGLE_POOL_ID, gauge);
    }

    function testRevertOnUnauthorizedSetGaugeOf() public {
        vm.expectRevert();
        vm.prank(address(42));
        reliquary.setGaugeOf(PAIR_POOL_ID, gauge);
    }

    function testRevertOnAuthorizedHarvestGaugeRewards() public {
        address[] memory tokens = new address[](1);
        tokens[0] = address(farmingToken);
        vm.expectRevert();
        vm.prank(address(42));
        reliquary.harvestGaugeRewards(gauge, tokens);
    }

    function testHarvestFarmRewards(
        uint amountSingle,
        uint amountPair,
        uint amountOtherPair,
        uint amountStakingReward0,
        uint amountStakingReward1
    ) public {
        amountStakingReward0 = bound(amountStakingReward0, 1, type(uint).max / 3);
        amountStakingReward1 = bound(amountStakingReward1, 1, type(uint).max / 3);
        vm.assume(amountStakingReward0 + amountStakingReward1 + 0.42 ether <= farmingToken.balanceOf(address(this)));

        ERC20DecimalsMock otherPairToken = new ERC20DecimalsMock("Other Pair", "OTHERPAIR", 18);
        otherPairToken.mint(address(this), 1_000_000 ether);
        otherPairToken.approve(address(reliquary), type(uint).max);

        address otherGauge = makeGauge(address(otherPairToken));

        reliquary.addPool(
            200,
            address(otherPairToken),
            address(0),
            requiredMaturity,
            allocPoints,
            "OTHER PAIR Pool",
            nftDescriptor,
            true
        );

        reliquary.setGaugeOf(2, address(otherGauge));

        amountSingle = bound(amountSingle, 1, singleToken.balanceOf(address(this)));
        amountPair = bound(amountPair, 1, pairToken.balanceOf(address(this)));
        amountOtherPair = bound(amountOtherPair, 1, otherPairToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amountSingle);
        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountPair);
        reliquary.createRelicAndDeposit(address(this), 2, amountOtherPair);

        // Ensure pair tokens have been staked in their gauges
        assertEq(pairToken.balanceOf(gauge), amountPair);
        assertEq(otherPairToken.balanceOf(address(otherGauge)), amountOtherPair);

        // Gauge rewards to be harvested
        farmingToken.transfer(gauge, amountStakingReward0);
        farmingToken.transfer(address(otherGauge), amountStakingReward1);

        // Put some in the reliquary to test they will be transfered
        farmingToken.transfer(address(reliquary), 0.42 ether);

        reliquary.grantRole(keccak256("OPERATOR"), address(1));

        address[] memory tokens = new address[](1);
        tokens[0] = address(farmingToken);

        vm.prank(address(1));
        reliquary.harvestGaugeRewards(gauge, tokens);

        vm.prank(address(1));
        reliquary.harvestGaugeRewards(address(otherGauge), tokens);

        // Gauge rewards have been harvested:
        assertEq(farmingToken.balanceOf(address(1)), amountStakingReward0 + amountStakingReward1 + 0.42 ether);
        // Reliquary initial balance is still there:
        assertEq(farmingToken.balanceOf(address(reliquary)), 0);
    }
}

contract ReliquaryPrimeV2VelodromeTest is ReliquaryPrimeV2Test {
    function setUpReliquary() override internal {
        address curve = address(new Constant());
        reliquary = new ReliquaryPrimeV2Velodrome(address(rewardToken), curve, "Reliquary Prime V2", "RELICPRIMEV2");
        gauge = address(new VelodromeV2Gauge(address(farmingToken), address(pairToken)));
    }

    function makeGauge(address stakingToken) override internal returns (address) {
        return address(new VelodromeV2Gauge(address(farmingToken), stakingToken));
    }
}

// Simulate a Solidly-fork gauge, where the deposit function takes a tokenId
// This one assume a single user and that earned rewards are fully distributed instantly.
contract VelodromeV2Gauge is IVelodromeV2Gauge {
    address public rewardToken;
    address public stakingToken;

    constructor(address rewardToken_, address stakingToken_) {
        rewardToken = rewardToken_;
        stakingToken = stakingToken_;
    }

    function getReward(address account) external {
        IERC20(rewardToken).transfer(account, IERC20(rewardToken).balanceOf(address(this)));
    }

    function deposit(uint amount) external {
        require(amount > 0);
        IERC20(stakingToken).transferFrom(msg.sender, address(this), amount);
    }

    function withdraw(uint amount) external {
        IERC20(stakingToken).transfer(msg.sender, amount);
    }

    function balanceOf(address /*owner*/ ) external view returns (uint) {
        return IERC20(stakingToken).balanceOf(address(this));
    }
}

contract ReliquaryPrimeV2EqualizerTest is ReliquaryPrimeV2Test {
    function setUpReliquary() override internal {
        address curve = address(new Constant());
        reliquary = new ReliquaryPrimeV2Equalizer(address(rewardToken), curve, "Reliquary Prime V2", "RELICPRIMEV2");
        gauge = address(new EqualizerGauge(address(pairToken)));
    }

    function makeGauge(address stakingToken) override internal returns (address) {
        return address(new EqualizerGauge(stakingToken));
    }

    function testHarvestMultiFarmRewards(
        uint amountPair,
        uint amountFarmingReward0,
        uint amountFarmingReward1
    ) public {
        amountFarmingReward0 = bound(amountFarmingReward0, 1, type(uint).max / 3);
        amountFarmingReward1 = bound(amountFarmingReward1, 1, type(uint).max / 3);
        vm.assume(amountFarmingReward0 <= farmingToken.balanceOf(address(this)));

        ERC20DecimalsMock otherFarmToken = new ERC20DecimalsMock("Other Farm", "OTHERFARM", 18);
        otherFarmToken.mint(address(this), 1_000_000 ether);
        vm.assume(amountFarmingReward1 <= otherFarmToken.balanceOf(address(this)));

        amountPair = bound(amountPair, 1, pairToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountPair);

        // Ensure pair tokens have been staked in their gauges
        assertEq(pairToken.balanceOf(gauge), amountPair);

        // Gauge rewards to be harvested
        farmingToken.transfer(gauge, amountFarmingReward0);
        otherFarmToken.transfer(gauge, amountFarmingReward1);

        reliquary.grantRole(keccak256("OPERATOR"), address(1));

        address[] memory tokens = new address[](2);
        tokens[0] = address(farmingToken);
        tokens[1] = address(otherFarmToken);

        vm.prank(address(1));
        reliquary.harvestGaugeRewards(gauge, tokens);

        // Gauge rewards have been harvested:
        assertEq(farmingToken.balanceOf(address(1)), amountFarmingReward0);
        assertEq(otherFarmToken.balanceOf(address(1)), amountFarmingReward1);
        // Reliquary initial balance is still there:
        assertEq(farmingToken.balanceOf(address(reliquary)), 0);
        assertEq(otherFarmToken.balanceOf(address(reliquary)), 0);
    }
}

// Simulate a Solidly-fork gauge, where the deposit function takes a tokenId
// This one assume a single user and that earned rewards are fully distributed instantly.
contract EqualizerGauge is IEqualizerGauge {
    address public stake;

    constructor(address stake_) {
        stake = stake_;
    }

    function getReward(address account, address[] memory tokens) external {
        for (uint i = 0; i < tokens.length; ++i) {
            IERC20(tokens[i]).transfer(account, IERC20(tokens[i]).balanceOf(address(this)));
        }
    }

    function deposit(uint amount) external {
        require(amount > 0);
        IERC20(stake).transferFrom(msg.sender, address(this), amount);
    }

    function withdraw(uint amount) external {
        IERC20(stake).transfer(msg.sender, amount);
    }

    function balanceOf(address /*owner*/ ) external view returns (uint) {
        return IERC20(stake).balanceOf(address(this));
    }
}
