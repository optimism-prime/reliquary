// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "contracts/ReliquaryPrime.sol";
import "contracts/emission_curves/Constant.sol";
import "contracts/nft_descriptors/NFTDescriptor.sol";
import "openzeppelin-contracts/contracts/token/ERC721/utils/ERC721Holder.sol";
import "openzeppelin-contracts/contracts/mocks/ERC20DecimalsMock.sol";
import "openzeppelin-contracts/contracts/mocks/ERC721Mock.sol";
import "openzeppelin-contracts/contracts/mocks/ERC1155Mock.sol";

contract ReliquaryPrimeTest is ERC721Holder, Test {
    ReliquaryPrime reliquary;

    uint constant SINGLE_POOL_ID = 0;
    uint constant PAIR_POOL_ID = 1;

    ERC20DecimalsMock rewardToken;
    ERC20DecimalsMock pairToken;
    ERC20DecimalsMock singleToken;
    ERC20DecimalsMock VELO;
    address nftDescriptor;

    uint[] requiredMaturity = [0, 1 days, 7 days, 14 days, 30 days, 90 days, 180 days, 365 days];
    uint[] allocPoints = [100, 120, 150, 200, 300, 400, 500, 750];

    Gauge gauge;

    function setUp() public {
        rewardToken = new ERC20DecimalsMock("Reward", "REWARD", 18);
        pairToken = new ERC20DecimalsMock("Pair", "PAIR", 18);
        singleToken = new ERC20DecimalsMock("Single", "SINGLE", 18);
        VELO = new ERC20DecimalsMock("Staking Reward", "STKREWARD", 18);

        address curve = address(new Constant());
        reliquary = new ReliquaryPrime(address(rewardToken), curve, "Reliquary Prime", "RELICPRIME", address(VELO));
        nftDescriptor = address(new NFTDescriptor(address(reliquary)));

        reliquary.grantRole(keccak256("OPERATOR"), address(this));
        reliquary.addPool(
            100, address(singleToken), address(0), requiredMaturity, allocPoints, "SINGLE Pool", nftDescriptor, true
        );
        reliquary.addPool(
            200, address(pairToken), address(0), requiredMaturity, allocPoints, "SINGLE Pool", nftDescriptor, true
        );

        gauge = new Gauge(VELO, pairToken);

        reliquary.setGaugeOf(PAIR_POOL_ID, address(gauge));

        rewardToken.mint(address(this), 100_000_000 ether);
        VELO.mint(address(this), 100_000_000 ether);

        singleToken.mint(address(this), 100_000_000 ether);
        singleToken.approve(address(reliquary), type(uint).max);

        pairToken.mint(address(this), 100_000_000 ether);
        pairToken.approve(address(reliquary), type(uint).max);
    }

    function testPairTokenStakedWhenCreateRelicAndDeposit(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        // We expect amount pairToken to have been staked in the gauge
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(address(gauge)), amount);
    }

    function testPairTokenStakedWhenDepositExisting(uint amountA, uint amountB) public {
        amountA = bound(amountA, 1, type(uint).max / 2);
        amountB = bound(amountB, 1, type(uint).max / 2);
        vm.assume(amountA + amountB <= pairToken.balanceOf(address(this)));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountA);
        reliquary.deposit(amountB, relicId);
        assertEq(reliquary.getPositionForId(relicId).amount, amountA + amountB);

        // We expect amountA + amountB pairTokens to have been staked in the gauge
        assertEq(pairToken.balanceOf(address(reliquary)), 0);
        assertEq(pairToken.balanceOf(address(gauge)), amountA + amountB);
    }

    function testSingleTokenNotStakedWhenCreateRelicAndDeposit(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        // We expect amount singleToken to remain in the reliquary
        assertEq(singleToken.balanceOf(address(reliquary)), amount);
    }

    function testSingleTokenNotStakedWhenDepositExisting(uint amountA, uint amountB) public {
        amountA = bound(amountA, 1, type(uint).max / 2);
        amountB = bound(amountB, 1, type(uint).max / 2);
        vm.assume(amountA + amountB <= singleToken.balanceOf(address(this)));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amountA);
        reliquary.deposit(amountB, relicId);
        assertEq(reliquary.getPositionForId(relicId).amount, amountA + amountB);

        // We expect amountA + amountB singleTokens to remain in the reliquary
        assertEq(singleToken.balanceOf(address(reliquary)), amountA + amountB);
    }

    function testPairTokenUnstakedWhenWithdraw(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdraw(amount, relicId);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testPairTokenUnstakedWhenWithdrawAndHarvest(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdrawAndHarvest(amount, relicId, address(gauge));

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenWithdraw(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdraw(amount, relicId);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenWithdrawAndHarvest(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.withdrawAndHarvest(amount, relicId, address(gauge));

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testSingleTokenEmergencyWithdraw(uint amount) public {
        amount = bound(amount, 1, singleToken.balanceOf(address(this)));

        uint balanceBefore = singleToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amount);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.emergencyWithdraw(relicId);

        assertEq(singleToken.balanceOf(address(this)), balanceBefore);
    }

    function testPairTokenEmergencyWithdraw(uint amount) public {
        amount = bound(amount, 1, pairToken.balanceOf(address(this)));

        uint balanceBefore = pairToken.balanceOf(address(this));
        uint relicId = reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amount);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore - amount);

        reliquary.emergencyWithdraw(relicId);

        assertEq(pairToken.balanceOf(address(this)), balanceBefore);
    }

    function testRevertOnGaugeStakeTokenMismatch() public {
        vm.expectRevert(ReliquaryPrime.GaugeStakeTokenMismatch.selector);
        reliquary.setGaugeOf(SINGLE_POOL_ID, address(gauge));
    }

    function testHarvestFarmRewards(
        uint amountSingle,
        uint amountPair,
        uint amountOtherPair,
        uint amountStakingReward0,
        uint amountStakingReward1
    ) public {
        amountStakingReward0 = bound(amountStakingReward0, 1, type(uint).max / 3);
        amountStakingReward1 = bound(amountStakingReward1, 1, type(uint).max / 3);
        vm.assume(amountStakingReward0 + amountStakingReward1 + 0.42 ether <= VELO.balanceOf(address(this)));

        ERC20DecimalsMock otherPairToken = new ERC20DecimalsMock("Other Pair", "OTHERPAIR", 18);
        otherPairToken.mint(address(this), 1_000_000 ether);
        otherPairToken.approve(address(reliquary), type(uint).max);

        Gauge otherGauge = new Gauge(VELO, otherPairToken);

        reliquary.addPool(
            200,
            address(otherPairToken),
            address(0),
            requiredMaturity,
            allocPoints,
            "OTHER PAIR Pool",
            nftDescriptor,
            true
        );

        reliquary.setGaugeOf(2, address(otherGauge));

        amountSingle = bound(amountSingle, 1, singleToken.balanceOf(address(this)));
        amountPair = bound(amountPair, 1, pairToken.balanceOf(address(this)));
        amountOtherPair = bound(amountOtherPair, 1, otherPairToken.balanceOf(address(this)));
        reliquary.createRelicAndDeposit(address(this), SINGLE_POOL_ID, amountSingle);
        reliquary.createRelicAndDeposit(address(this), PAIR_POOL_ID, amountPair);
        reliquary.createRelicAndDeposit(address(this), 2, amountOtherPair);

        // Ensure pair tokens have been staked in their gauges
        assertEq(pairToken.balanceOf(address(gauge)), amountPair);
        assertEq(otherPairToken.balanceOf(address(otherGauge)), amountOtherPair);

        // Gauge rewards to be harvested
        VELO.transfer(address(gauge), amountStakingReward0);
        VELO.transfer(address(otherGauge), amountStakingReward1);

        // Put some in the reliquary to test they won't be transfered
        VELO.transfer(address(reliquary), 0.42 ether);

        reliquary.grantRole(keccak256("OPERATOR"), address(1));
        vm.prank(address(1));

        reliquary.harvestVelodromeRewards();

        // Gauge rewards have been harvested:
        assertEq(VELO.balanceOf(address(1)), amountStakingReward0 + amountStakingReward1);
        // Reliquary initial balance is still there:
        assertEq(VELO.balanceOf(address(reliquary)), 0.42 ether);
    }

    // function testRevertOnRescueERC20WithRewardToken(uint amount) public {
    //   amount = bound(amount, 1, rewardToken.balanceOf(address(this)));

    //   uint balanceBefore = rewardToken.balanceOf(address(this));
    //   rewardToken.transfer(address(reliquary), amount);

    //   assertEq(rewardToken.balanceOf(address(this)), balanceBefore - amount);

    //   vm.expectRevert(ReliquaryPrime.RewardTokenCannotBeRescued.selector);
    //   reliquary.rescueERC20(rewardToken, amount);
    // }

    // function testRevertOnRescueERC20WithPoolToken(uint amount) public {
    //   amount = bound(amount, 1, singleToken.balanceOf(address(this)));

    //   uint balanceBefore = singleToken.balanceOf(address(this));
    //   singleToken.transfer(address(reliquary), amount);

    //   assertEq(singleToken.balanceOf(address(this)), balanceBefore - amount);

    //   vm.expectRevert(ReliquaryPrime.PoolTokenCannotBeRescued.selector);
    //   reliquary.rescueERC20(singleToken, amount);
    // }

    // function testRescueERC20(uint amount) public {
    //   amount = bound(amount, 1, VELO.balanceOf(address(this)));

    //   uint balanceBefore = VELO.balanceOf(address(this));
    //   VELO.transfer(address(reliquary), amount);

    //   assertEq(VELO.balanceOf(address(this)), balanceBefore - amount);

    //   reliquary.rescueERC20(VELO, amount);

    //   assertEq(VELO.balanceOf(address(this)), balanceBefore);
    // }

    // function testRescueERC721() public {
    //   ERC721Mock token = new ERC721Mock("ERC721", "ERC721");
    //   token.mint(address(reliquary), 0);

    //   assertEq(token.ownerOf(0), address(reliquary));

    //   reliquary.rescueERC721(token, 0);

    //   assertEq(token.ownerOf(0), address(this));
    // }
}

// Simulate a Solidly-fork gauge, where the deposit function takes a tokenId
// This one assume a single user and that earned rewards are fully distributed instantly.
contract Gauge is IVelodromeGauge {
    ERC20DecimalsMock public VELO;
    ERC20DecimalsMock public stakeToken;

    constructor(ERC20DecimalsMock stakingRewardToken_, ERC20DecimalsMock stakeToken_) {
        VELO = stakingRewardToken_;
        stakeToken = stakeToken_;
    }

    function getReward(address account, address[] memory /*tokens*/ ) external {
        VELO.transfer(account, VELO.balanceOf(address(this)));
    }

    function deposit(uint amount, uint /*tokenId*/ ) external {
        stakeToken.transferFrom(msg.sender, address(this), amount);
    }

    function withdraw(uint amount) external {
        stakeToken.transfer(msg.sender, amount);
    }

    function stake() external view returns (address) {
        return address(stakeToken);
    }

    function balanceOf(address /*owner*/ ) external view returns (uint) {
        return stakeToken.balanceOf(address(this));
    }

    function earned(address, /*token*/ address /*account*/ ) external view returns (uint) {
        return VELO.balanceOf(address(this));
    }
}
