# Reliquary Prime

The ReliquaryPrime is an adaptation of the Reliquary from Byte Masons (see [section below](#Reliquary)).

It is used by [Optimism Prime project](https://optimismprime.io/) to distribute a share of treasury yield to long term stakers of key assets.
Some of these key assets are Velodrome pairs, so the ReliquaryPrime stake them on Velodrome to earn and lock VELO
so it keeps growing treasury yield for stakers.

In order to implement the Velodrome staking workflow, some changes were required in the Reliquary base contract.

These changes were:
- Changing some functions into virtual and/or internal, so we can override them and implement the workflow
  - Depositing required changing _deposit
  - Withdrawing required changing _updatePosition
  - Emergency withdrawing required changing emergencyWithdraw
- Removing some functions to reduce contract byte size and allow deployment onchain
  - pendingRewardsOfOwner and relicPositionsOfOwner have been removed
  - these two functions can be implemented off chain, or in a helper contract

## Deployments

Deployment is done with `hardhat-deploy` and an history of contracts is kept in `deployments/deployOptimism`.

### 2023-05-10

The first production Reliquary Prime has been deployed the 2023-05-10. The reward token is [OP](https://optimistic.etherscan.io/token/0x4200000000000000000000000000000000000042).

- OwnableCurve deployment: https://optimistic.etherscan.io/tx/0x49a9b0843738758d2a709a969e9e49e0812b052cb66096773850ae84a30c0c99
  - Deployment address: [0xfe5116d830485b2c7b7cb1d9ff9cee15f99dcf79](https://optimistic.etherscan.io/address/0xfe5116d830485b2c7b7cb1d9ff9cee15f99dcf79#code)
- ReliquaryPrime deployment with [OP](https://optimistic.etherscan.io/token/0x4200000000000000000000000000000000000042) as reward token: https://optimistic.etherscan.io/tx/0x2be52741442e48ca62f3e3657243fcba8705ca2a23249ba49d0053692a4e4f40
  - Deployment address: [0xb6372b2b157fb80703c985c19a41f76dcbbd4b71](https://optimistic.etherscan.io/address/0xb6372b2b157fb80703c985c19a41f76dcbbd4b71)

OPERATOR role has been granted to a 1/1 [gnosis safe](https://app.safe.global/home?safe=oeth:0x047C8F60F489ec9245F0cEeeb62b676e60a1E576) to easily setup pools in a single transaction using the transaction builder.

- Granting OPERATOR role: https://optimistic.etherscan.io/tx/0xad02a815acc638ecbd4d22a2d5c59cf101afe763055aad756fddfc47b95286a0

Pools were created with a single transaction executed from the safe:

- Transaction: https://optimistic.etherscan.io/tx/0x852508780c3cedc55f0dd6e54c43432406fd732501c604d8073b48c8814d8abd
- [JSON file describing the transaction](deployments/_archive/deployOptimism/2023-05-10-ReliquaryPrime_OPRewards_AddPools/001_ReliquaryPrime_OPRewards_AddPools_TxBuilder.json)

OPERATOR role has been revoked on the safe: https://optimistic.etherscan.io/tx/0x24ebc8d2d51169e3be3ef9a9d970bdca28622a96e3a50ddd16a24391f5489392

DEFAULT_ADMIN_ROLE and OPERATOR roles have been granted to the [Optimism Prime treasury multi sig](https://app.safe.global/home?safe=oeth:0x1481BffbB3a825AC994DBdDCEf1eC2082fE35a3D):

- OPERATOR: https://optimistic.etherscan.io/tx/0xa4368743aa7b9dec5e1884063f4417b485768d465b7a88f088d68f74e8dcf3ae
- DEFAULT_ADMIN_ROLE: https://optimistic.etherscan.io/tx/0xc77c96aae2336b4a39c915e45dc78fe061cfb79c0b9ea016189183989878413e

DEFAULT_ADMIN_ROLE has been revoked from the EOA deployer address: https://optimistic.etherscan.io/tx/0x31cc08294905619adeaa5cf474d00f2c31c868540698cbf83dcd77b38ec19315

Ownership of the emission curve contract was transfered to our multi-sig: https://optimistic.etherscan.io/tx/0x9db18657fafe389397a9418f42c831a81a53925a0c79b94c05fcb7cff8084ba0

# Autobribes

Autobribes protocol from Optimism Prime is another instance of the Reliquary deployed for pairs not related to OPP token.

## Velodrome

This protocol will have its own Velodrome veNFT to accumulate, lock VELO and then redistribute bribes to stakers.

### Deployments

#### 2023-05-13

- OwnableCurve deployment with 0 rate: https://optimistic.etherscan.io/tx/0x429c849a9092c7a09250bfb3657d25be3cee524fcb8a1d9e7cbe90d18687ce20
  - Deployment address: [0x781009376c3aec23a4787f75f9417d3172c3f676](https://optimistic.etherscan.io/address/0x781009376c3aec23a4787f75f9417d3172c3f676#code)
- ReliquaryPrime deployment with [WETH](https://optimistic.etherscan.io/address/0x4200000000000000000000000000000000000006) as reward token: https://optimistic.etherscan.io/tx/0x429c849a9092c7a09250bfb3657d25be3cee524fcb8a1d9e7cbe90d18687ce20
  - Deployment address: [0x5a863113ac76000faedaaf6a0abf02c21130e3e3](https://optimistic.etherscan.io/address/0x5a863113ac76000faedaaf6a0abf02c21130e3e3#readContract)

#### 2023-05-31

OPERATOR role has been granted to an EOA for admin:

- https://optimistic.etherscan.io/tx/0x641e2667ef34469ebca70b99a9d60f958100fdb279d94661d33f26a6b921cd64

[ETH/USDC](https://optimistic.etherscan.io/token/0x79c912FEF520be002c2B6e57EC4324e260f38E50) pool has been created:

- https://optimistic.etherscan.io/tx/0xf8d968e82192fbf5cc7130eec8a09636b45655f6a6f8c9039383c7233fe1e3bc

[Gauge](https://optimistic.etherscan.io/address/0xE2CEc8aB811B648bA7B1691Ce08d5E800Dd0a60a) has been assigned:

- https://optimistic.etherscan.io/tx/0x8a9aed06532b6647bc972d71fb0ea25ac7473b696b9ddd3a6c5f746e8c74b4e9

## Equalizer

For Equalizer we had to slightly change to code to handle the fact the `deposit()` does'nt take a tokenId as argument (which is actually better, cause unused anyway for Velodrome).

The quick'n dirty solution was just to duplicate the contract and do the few changes. But a better solution would be to have a generic system where the Reliquary does not care about that, it should just deposit, withdraw and harvest.

### Deployments

#### 2023-06-02

- OwnableCurve deployment with 0 rate: https://ftmscan.com/tx/0xcfb74f2c66a6a6fea95b30bd19f3e18ecf6830a10bdcab7a04a4ce9e0044cecb
  - Deployment address: [0x1dd41C013cbcac5239DA64C90f7a1a3F4FA45B14](https://ftmscan.com/address/0x1dd41c013cbcac5239da64c90f7a1a3f4fa45b14#code)
- ReliquaryAutobribesEqualizer deployment with [WFTM](https://ftmscan.com/token/0x21be370d5312f44cb42ce377bc9b8a0cef1a4c83) as reward token: https://ftmscan.com/tx/0xd7b56c774ec01f09d00d17894c7ddf35582b7f9910ce74500b08b222a9ee959d
  - Deployment address: [0x7a05B1f6a3df87AD74422080E0d1a1B733a46D46](https://ftmscan.com/address/0x7a05b1f6a3df87ad74422080e0d1a1b733a46d46#code)

OPERATOR role has been granted to an EOA for admin:

- https://ftmscan.com/tx/0xe052ba63d4ea60b706ff1afb3bccada6d52f3af9779fd5de7a99b13000ab6b67

[FTM/USDC](https://ftmscan.com/token/0x7547d05dFf1DA6B4A2eBB3f0833aFE3C62ABD9a1) pool has been created:

- https://ftmscan.com/tx/0x7187b134694597de4c9754d7bf652345b1d0e0cde3b6e6704dafb08ce7ca6724

[Gauge](https://ftmscan.com/address/0x48afe4b50aadbc09d0bceb796d9e956ea90f15b4) has been assigned:

- https://ftmscan.com/tx/0x853ab89d55f9a7f2d54cda7611932a66d419cfa3066e2241c9bff347aba0985f

# Reliquary

![Reliquary](header.png "Reliquary")

> Designed and written by [Justin Bebis](https://twitter.com/0xBebis_) and Zokunei, with help from [Goober](https://twitter.com/0xGoober) and the rest of the [Byte Masons](https://twitter.com/ByteMasons) crew.
---
Reliquary is a smart contract system that is designed to improve outcomes of incentive distribution by giving users and developers fine grained control over their investments and rewards. It accomplishes this with the following features:
1) Emits tokens based on the maturity of a user's investment, separated in tranches.
2) Binds variable emission rates to a base emission curve designed by the developer for predictable emissions.
3) Supports deposits and withdrawals along with these variable rates, which has historically been impossible.
4) Issues a 'financial NFT' to users which represents their underlying positions, able to be traded and leveraged without removing the underlying liquidity.
5) Can emit multiple types of rewards for each investment as well as handle complex reward mechanisms based on deposit and withdrawal.

By binding tokens to a base emission rate you not only gain the advantage of a predictable emission curve, but you're able
to get extremely creative with the Curve contracts you write. Whether this be a sigmoid curve, a square root curve, or a
random curve, you can codify the user behaviors you'd like to promote.

Please reach out to zokunei@bytemasons.com to report bugs or other funky behavior. We will proceed with various stages of production
testing in the coming weeks.


## Installation
This is a Foundry project. Get Foundry from [here](https://github.com/foundry-rs/foundry).

Please run the following command in this project's root directory to enable pre-commit testing:

```bash
ln -s ../../pre-commit .git/hooks/pre-commit
```
