// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./ReliquaryPrimeV2Base.sol";

contract ReliquaryPrimeV2Velodrome is ReliquaryPrimeV2Base {
    using SafeERC20 for IERC20;

    constructor(address _rewardToken, address _emissionCurve, string memory name, string memory symbol)
        ReliquaryPrimeV2Base(_rewardToken, _emissionCurve, name, symbol) {
    }

    function _withdrawFromOldGaugeAndDepositInNewGauge(uint poolId, address gauge) override internal {
        address oldGauge = gaugeOf[poolId];
        if (oldGauge != address(0)) {
            IVelodromeV2Gauge(oldGauge).withdraw(IVelodromeV2Gauge(oldGauge).balanceOf(address(this)));
        }

        if (gauge != address(0)){
            uint amountToDeposit = IERC20(poolToken[poolId]).balanceOf(address(this));
            if (amountToDeposit > 0) {
                IERC20(poolToken[poolId]).safeApprove(gauge, amountToDeposit);
                IVelodromeV2Gauge(gauge).deposit(amountToDeposit);
            }
        }
    }

    function _harvestGaugeRewards(address gauge, address[] memory /*tokens*/) override internal {
        IVelodromeV2Gauge(gauge).getReward(address(this));
        IERC20 rewardToken = IERC20(IVelodromeV2Gauge(gauge).rewardToken());
        rewardToken.safeTransfer(msg.sender, rewardToken.balanceOf(address(this)));
    }

    function gaugeStakingToken(address gauge) public view override returns(address) {
        return IVelodromeV2Gauge(gauge).stakingToken();
    }

    function _withdrawFromGauge(address gauge, uint amount) override internal {
        IVelodromeV2Gauge(gauge).withdraw(amount);
    }

    function _depositInGauge(address gauge, uint amount) override internal {
        IVelodromeV2Gauge(gauge).deposit(amount);
    }
}

interface IVelodromeV2Gauge {
    function getReward(address account) external;

    function deposit(uint amount) external;

    function withdraw(uint amount) external;

    function stakingToken() external view returns (address); // Returns the LP token to stake

    function rewardToken() external view returns (address);

    function balanceOf(address owner) external view returns (uint);
}
