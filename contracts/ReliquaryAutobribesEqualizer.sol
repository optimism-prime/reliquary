// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Reliquary.sol";

// Same as ReliquaryPrime (which is originally crafted for Velodrome) but deposit() as no tokenId argument on Equalizer
// TODO Factorize this
contract ReliquaryAutobribesEqualizer is Reliquary {
    using SafeERC20 for IERC20;

    mapping(uint => address) public gaugeOf; // The gauge of each asset
    IERC20 public dexToken;

    error GaugeStakeTokenMismatch();
    error VELOAsPoolToken();
    error RewardTokenCannotBeRescued();
    error PoolTokenCannotBeRescued();

    constructor(address _rewardToken, address _emissionCurve, string memory name, string memory symbol, address _dexToken)
        Reliquary(_rewardToken, _emissionCurve, name, symbol)
    {
        dexToken = IERC20(_dexToken);
    }

    // Equalizer specific:

    function setGaugeOf(uint poolId, address gauge) external onlyRole(OPERATOR) {
        if (poolId >= poolLength()) revert NonExistentPool();
        if (IEqualizerGauge(gauge).stake() != poolToken[poolId]) { // Safety check
            revert GaugeStakeTokenMismatch();
        }
        gaugeOf[poolId] = gauge;
    }

    function harvestDexRewards() external onlyRole(OPERATOR) {
        uint balanceBefore = dexToken.balanceOf(address(this));
        address[] memory tokens = new address[](1);
        tokens[0] = address(dexToken);
        uint poolCount = poolLength();
        for (uint poolId = 0; poolId < poolCount; ++poolId) {
            address gauge = gaugeOf[poolId];
            if (gauge != address(0)) {
                IEqualizerGauge(gauge).getReward(address(this), tokens);
            }
        }
        uint balanceAfter = dexToken.balanceOf(address(this));
        dexToken.safeTransfer(msg.sender, balanceAfter - balanceBefore);
    }

    // Reliquary adaptations:

    // For withdraw we override _updatePosition to unstake asset if required, before
    // they are transfered to the caller (in withdraw() and withdrawAndHarvest())
    function _updatePosition(uint amount, uint relicId, Kind kind, address harvestTo)
        internal
        override
        returns (uint poolId, uint _pendingReward)
    {
        if (kind == Kind.WITHDRAW) {
            PositionInfo storage position = positionForId[relicId];
            poolId = position.poolId;
            address gauge = gaugeOf[poolId];
            if (gauge != address(0)) {
                IEqualizerGauge(gauge).withdraw(amount);
            }
        }
        (poolId, _pendingReward) = super._updatePosition(amount, relicId, kind, harvestTo);
    }

    // The emergencyWithdraw function doesn't use _updatePosition so we need to override it.
    // We first extract LPs from Velodrome if required and then call the original function
    // that have been moved into _emergencyWithdraw.
    function emergencyWithdraw(uint relicId) external override nonReentrant {
        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;
        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            IEqualizerGauge(gauge).withdraw(position.amount);
        }
        super._emergencyWithdraw(relicId);
    }

    // For deposit we override _deposit to stake the assets after they are transfered
    // to the contract (in createRelicAndDeposit() and deposit())
    function _deposit(uint amount, uint relicId) internal override {
        super._deposit(amount, relicId);

        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;

        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            IERC20(poolToken[poolId]).safeApprove(gauge, amount);
            IEqualizerGauge(gauge).deposit(amount);
        }
    }
}

interface IEqualizerGauge {
    function getReward(address account, address[] memory tokens) external;

    function deposit(uint amount) external;

    function withdraw(uint amount) external;

    function stake() external view returns (address); // Returns the LP token to stake
}
