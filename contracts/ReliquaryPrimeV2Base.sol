// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Reliquary.sol";

// The ReliquaryPrime is an adaptation of the Reliquary from Byte Masons.
//
// It is used by Optimism Prime project to distribute a share of treasury yield to long term stakers of key assets.
// Some of these key assets are LP tokens, so the ReliquaryPrime stake them on gauges to earn and lock a farming token
// so it keeps growing treasury yield for stakers.
//
// In order to implement the farming tokens staking workflow, some changes were required in the Reliquary base contract.
// These changes were:
// - Changing some functions into virtual and/or internal, so we can override them and implement the workflow
//     - Depositing required changing _deposit
//     - Withdrawing required changing _updatePosition
//     - Emergency withdrawing required changing emergencyWithdraw
// - Removing some functions to reduce contract byte size and allow deployment onchain
//     - pendingRewardsOfOwner and relicPositionsOfOwner have been removed
//     - these two functions can be implemented off chain, or in a helper contract
abstract contract ReliquaryPrimeV2Base is Reliquary {
    using SafeERC20 for IERC20;

    mapping(uint => address) public gaugeOf; // The farming gauge of each asset

    error GaugeStakeTokenMismatch();

    constructor(address _rewardToken, address _emissionCurve, string memory name, string memory symbol)
        Reliquary(_rewardToken, _emissionCurve, name, symbol)
    {
    }

    // gauge specific:

    function setGaugeOf(uint poolId, address gauge) external onlyRole(OPERATOR) {
        if (poolId >= poolLength()) revert NonExistentPool();
        if (gauge != address(0) && gaugeStakingToken(gauge) != poolToken[poolId]) { // Safety check
            revert GaugeStakeTokenMismatch();
        }
        _withdrawFromOldGaugeAndDepositInNewGauge(poolId, gauge);

        gaugeOf[poolId] = gauge;
    }

    function harvestGaugeRewards(address gauge, address[] memory tokens) external onlyRole(OPERATOR) {
        _harvestGaugeRewards(gauge, tokens);
    }

    function _harvestGaugeRewards(address gauge, address[] memory tokens) virtual internal;
    function _withdrawFromOldGaugeAndDepositInNewGauge(uint poolId, address gauge) virtual internal;
    function _withdrawFromGauge(address gauge, uint amount) virtual internal;
    function _depositInGauge(address gauge, uint amount) virtual internal;

    function gaugeStakingToken(address gauge) public view virtual returns(address);

    // Reliquary adaptations:

    // For withdraw we override _updatePosition to unstake asset if required, before
    // they are transfered to the caller (in withdraw() and withdrawAndHarvest())
    function _updatePosition(uint amount, uint relicId, Kind kind, address harvestTo)
        internal
        override
        returns (uint poolId, uint _pendingReward)
    {
        if (kind == Kind.WITHDRAW) {
            PositionInfo storage position = positionForId[relicId];
            poolId = position.poolId;
            address gauge = gaugeOf[poolId];
            if (gauge != address(0)) {
                _withdrawFromGauge(gauge, amount);
            }
        }
        (poolId, _pendingReward) = super._updatePosition(amount, relicId, kind, harvestTo);
    }

    // The emergencyWithdraw function doesn't use _updatePosition so we need to override it.
    // We first extract LPs from gauge if required and then call the original function
    // that have been moved into _emergencyWithdraw.
    function emergencyWithdraw(uint relicId) external override nonReentrant {
        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;
        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            _withdrawFromGauge(gauge, position.amount);
        }
        super._emergencyWithdraw(relicId);
    }

    // For deposit we override _deposit to stake the assets after they are transfered
    // to the contract (in createRelicAndDeposit() and deposit())
    function _deposit(uint amount, uint relicId) internal override {
        super._deposit(amount, relicId);

        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;

        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            IERC20(poolToken[poolId]).safeApprove(gauge, amount);
            _depositInGauge(gauge, amount);
        }
    }
}
