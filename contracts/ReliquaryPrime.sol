// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Reliquary.sol";

// The ReliquaryPrime is an adaptation of the Reliquary from Byte Masons.
//
// It is used by Optimism Prime project to distribute a share of treasury yield to long term stakers of key assets.
// Some of these key assets are Velodrome pairs, so the ReliquaryPrime stake them on Velodrome to earn and lock VELO
// so it keeps growing treasury yield for stakers.
//
// In order to implement the Velodrome staking workflow, some changes were required in the Reliquary base contract.
// These changes were:
// - Changing some functions into virtual and/or internal, so we can override them and implement the workflow
//     - Depositing required changing _deposit
//     - Withdrawing required changing _updatePosition
//     - Emergency withdrawing required changing emergencyWithdraw
// - Removing some functions to reduce contract byte size and allow deployment onchain
//     - pendingRewardsOfOwner and relicPositionsOfOwner have been removed
//     - these two functions can be implemented off chain, or in a helper contract
contract ReliquaryPrime is Reliquary {
    using SafeERC20 for IERC20;

    mapping(uint => address) public gaugeOf; // The Velodrome gauge of each asset
    IERC20 public VELO;

    error GaugeStakeTokenMismatch();
    error VELOAsPoolToken();
    error RewardTokenCannotBeRescued();
    error PoolTokenCannotBeRescued();

    constructor(address _rewardToken, address _emissionCurve, string memory name, string memory symbol, address _VELO)
        Reliquary(_rewardToken, _emissionCurve, name, symbol)
    {
        VELO = IERC20(_VELO);
    }

    // Velodrome specific:

    function setGaugeOf(uint poolId, address gauge) external onlyRole(OPERATOR) {
        if (poolId >= poolLength()) revert NonExistentPool();
        if (IVelodromeGauge(gauge).stake() != poolToken[poolId]) { // Safety check
            revert GaugeStakeTokenMismatch();
        }
        gaugeOf[poolId] = gauge;
    }

    function harvestVelodromeRewards() external onlyRole(OPERATOR) {
        uint balanceBefore = VELO.balanceOf(address(this));
        address[] memory tokens = new address[](1);
        tokens[0] = address(VELO);
        uint poolCount = poolLength();
        for (uint poolId = 0; poolId < poolCount; ++poolId) {
            address gauge = gaugeOf[poolId];
            if (gauge != address(0)) {
                IVelodromeGauge(gauge).getReward(address(this), tokens);
            }
        }
        uint balanceAfter = VELO.balanceOf(address(this));
        VELO.safeTransfer(msg.sender, balanceAfter - balanceBefore);
    }

    // Reliquary adaptations:

    // For withdraw we override _updatePosition to unstake asset if required, before
    // they are transfered to the caller (in withdraw() and withdrawAndHarvest())
    function _updatePosition(uint amount, uint relicId, Kind kind, address harvestTo)
        internal
        override
        returns (uint poolId, uint _pendingReward)
    {
        if (kind == Kind.WITHDRAW) {
            PositionInfo storage position = positionForId[relicId];
            poolId = position.poolId;
            address gauge = gaugeOf[poolId];
            if (gauge != address(0)) {
                IVelodromeGauge(gauge).withdraw(amount);
            }
        }
        (poolId, _pendingReward) = super._updatePosition(amount, relicId, kind, harvestTo);
    }

    // The emergencyWithdraw function doesn't use _updatePosition so we need to override it.
    // We first extract LPs from Velodrome if required and then call the original function
    // that have been moved into _emergencyWithdraw.
    function emergencyWithdraw(uint relicId) external override nonReentrant {
        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;
        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            IVelodromeGauge(gauge).withdraw(position.amount);
        }
        super._emergencyWithdraw(relicId);
    }

    // For deposit we override _deposit to stake the assets after they are transfered
    // to the contract (in createRelicAndDeposit() and deposit())
    function _deposit(uint amount, uint relicId) internal override {
        super._deposit(amount, relicId);

        PositionInfo storage position = positionForId[relicId];
        uint poolId = position.poolId;

        address gauge = gaugeOf[poolId];
        if (gauge != address(0)) {
            IERC20(poolToken[poolId]).safeApprove(gauge, amount);
            IVelodromeGauge(gauge).deposit(amount, 0);
        }
    }
}

interface IVelodromeGauge {
    function getReward(address account, address[] memory tokens) external;

    function deposit(uint amount, uint tokenId) external;

    function withdraw(uint amount) external;

    function stake() external view returns (address); // Returns the LP token to stake
}
