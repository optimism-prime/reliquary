// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./ReliquaryPrimeV2Base.sol";

contract ReliquaryPrimeV2Equalizer is ReliquaryPrimeV2Base {
    using SafeERC20 for IERC20;

    constructor(address _rewardToken, address _emissionCurve, string memory name, string memory symbol)
        ReliquaryPrimeV2Base(_rewardToken, _emissionCurve, name, symbol) {
    }

    function _withdrawFromOldGaugeAndDepositInNewGauge(uint poolId, address gauge) override internal {
        address oldGauge = gaugeOf[poolId];
        if (oldGauge != address(0)) {
            IEqualizerGauge(oldGauge).withdraw(IEqualizerGauge(oldGauge).balanceOf(address(this)));
        }

        if (gauge != address(0)){
            uint amountToDeposit = IERC20(poolToken[poolId]).balanceOf(address(this));
            if (amountToDeposit > 0) {
                IERC20(poolToken[poolId]).safeApprove(gauge, amountToDeposit);
                IEqualizerGauge(gauge).deposit(amountToDeposit);
            }
        }
    }

    function _harvestGaugeRewards(address gauge, address[] memory tokens) override internal {
        IEqualizerGauge(gauge).getReward(address(this), tokens);
        for (uint i = 0; i < tokens.length; ++i){
            IERC20(tokens[i]).safeTransfer(msg.sender, IERC20(tokens[i]).balanceOf(address(this)));
        }
    }

    function gaugeStakingToken(address gauge) public view override returns(address) {
        return IEqualizerGauge(gauge).stake();
    }

    function _withdrawFromGauge(address gauge, uint amount) override internal {
        IEqualizerGauge(gauge).withdraw(amount);
    }

    function _depositInGauge(address gauge, uint amount) override internal {
        IEqualizerGauge(gauge).deposit(amount);
    }
}

interface IEqualizerGauge {
    function getReward(address account, address[] memory tokens) external;

    function deposit(uint amount) external;

    function withdraw(uint amount) external;

    function stake() external view returns (address); // Returns the LP token to stake

    function balanceOf(address owner) external view returns (uint);
}
